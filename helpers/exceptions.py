class OpenCategoryException(Exception):
    pass


class GetProductInCategoryException(Exception):
    pass


class ProductPopUpIsClosed(Exception):
    pass


class FailedAddProductToCart(Exception):
    pass


class CartIsEmpty(Exception):
    pass


class DeleteItemFromCartException(Exception):
    pass


class OpenProductPopUpException(Exception):
    pass


class ElementIsNotOpenedException(Exception):
    pass
