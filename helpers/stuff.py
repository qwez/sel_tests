def cut_last_slash(string):
    return string[:-1] if string[-1] == '/' else string


# WebDriverWait(driver, 1000).until(lambda driver: driver.title if 'Новая услуга' in driver.title else None)


def generator(func):
    item = func()
    while item:
        yield item
        item = func()
