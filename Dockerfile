FROM python:3.6.3-onbuild
CMD [ "py.test", "--domain=http://demo.litecart.net", "--hub=172.17.0.1:4444" ]