from selenium import webdriver
from pages.cart import ShoppingCart
from pages.product import ProductPopUp
from pages.categories import Categories
from blocks.prod_blocks import PopularProducts
from helpers import exceptions
import logging


class Application:

    shop = '/'
    admin = '/admin'

    def __init__(self, domain, selenium_hub, browser, enable_video=False):
        self.logger = logging.getLogger('my_logger')
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug('start create app')

        self.__executor = 'http://{}/wd/hub'.format(selenium_hub)
        browser, version = browser.split(':')
        self.__desired_capabilities = {'browserName': browser}
        if version:
            self.__desired_capabilities['version'] = version
        if enable_video:
            self.__desired_capabilities['enableVideo'] = enable_video
        self._domain = domain
        self.__driver = None
        self.__popular_products = None

    def __start_driver(self):
        if self.__driver:
            return
        self.__driver = webdriver.Remote(command_executor=self.__executor, desired_capabilities=self.__desired_capabilities)
        self.logger.debug('driver created = {}. Application = {}'.format(self.__driver, self))
        self.__driver.set_window_size(1400, 1000)
        self.cart = ShoppingCart(self.__driver, self._domain)
        self.categories = Categories(self.__driver, self._domain)
        self.product_popup = ProductPopUp(self.__driver)

    def open_shop(self):
        if not self.__driver:
            self.__start_driver()
        self.__driver.get(self._domain + self.shop)
        logging.info('open shop')
        return self

    def open_category(self, cat_name):
        if not self.__driver:
            self.__start_driver()
        return self.categories.open_category(cat_name)

    def open_popular_products(self):
        if not self.__driver:
            self.__start_driver()
        self.__popular_products = PopularProducts(self.__driver).open()
        return self.popular_products

    def close(self):
        logging.debug('quit')
        self.__driver.quit()

    @property
    def popular_products(self):
        if not self.__driver:
            self.__start_driver()
        if self.__popular_products is None:
            raise exceptions.ElementIsNotOpenedException('Popular products is not opened')
        return self.__popular_products
