from helpers import stuff, exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from pages.product import ProductPopUp


class ProductCard:

    def __init__(self, product_div, driver):
        self.__element = product_div
        self.__driver = driver
        self.__wait_u = WebDriverWait(driver, 10).until
        self.__product_popup = None

    def open_popup(self):
        self.__element.click()
        self.__product_popup = ProductPopUp(self.__driver)
        self.__wait_u(ec.visibility_of_element_located((By.CSS_SELECTOR, 'div.featherlight-content div.description')))
        return self.product_popup

    @property
    def name(self):
        return self.__element.find_element_by_css_selector('div.name').text

    @property
    def price(self):
        return ''

    @property
    def is_campaing_prod(self):
        return len(self.__element.find_elements_by_css_selector('div.price-wrapper *')) == 2

    @property
    def product_popup(self):
        if self.__product_popup is None:
            exceptions.ElementIsNotOpenedException('Product popup is not opened')
        return self.__product_popup
