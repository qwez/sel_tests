from helpers import stuff, exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from pages.product import ProductPopUp
from blocks.prod_card import ProductCard


class PopularProducts:

    def __init__(self, driver):
        self.__driver = driver
        self.__wait_u = WebDriverWait(self.__driver, 10).until
        self.last_get_product = None

    def open(self):
        self.__wait_u(ec.element_to_be_clickable((By.CSS_SELECTOR, 'main#content a[href="#popular-products"'))).click()
        self.__wait_u(ec.visibility_of_element_located((By.CSS_SELECTOR, 'div#popular-products')))
        self.last_get_product = 0
        return self

    def get_next_product(self):
        if not self.is_active or self.last_get_product is None:
            raise exceptions.OpenProductPopUpException('Popular Products block is not opened')
        all_products = self.__driver.find_elements_by_css_selector('div#box-popular-products div.product')
        if len(all_products) <= self.last_get_product:
            return None
        self.last_get_product += 1
        return ProductCard(all_products[self.last_get_product - 1], self.__driver)

    @property
    def is_active(self):
        return self.__driver.find_element_by_xpath(
            '//a[@href="#popular-products"]/..'
        ).get_attribute('class') == 'active'
