import pytest
from app.application import Application
import logging


logging.basicConfig(level=logging.ERROR,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        filename='log.log')

def pytest_addoption(parser):
    parser.addoption("--domain", action="store", help="domain", default='http://demo.litecart.net')
    parser.addoption("--hub", action="store", help="ip:port", default='172.17.0.1:4444')
    parser.addoption("--browser", help="list(browser_name:version)", default='chrome:', action="store")


def pytest_generate_tests(metafunc):
    metafunc.parametrize('browser_name', metafunc.config.getoption('browser').split(';'), scope='session')


@pytest.yield_fixture(scope='session')
def application(request, browser_name):
    domain = request.config.getoption('domain')
    selenium_hub = request.config.getoption('hub')
    browser = browser_name if ':' in browser_name else browser_name + ':'
    application = Application(domain, selenium_hub, browser)
    yield application
    application.close()