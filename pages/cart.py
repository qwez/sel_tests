from helpers import exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By


class ShoppingCart:

    def __init__(self, driver, domain):
        self.__driver = driver
        self._url = domain + '/checkout'
        self.__wait_u = WebDriverWait(self.__driver, 10).until

    def open(self):
        self.__driver.get(self._url)
        if not self.is_empty:
            self.__wait_u(ec.visibility_of_element_located(
                (By.CSS_SELECTOR, 'div#box-checkout-cart button[name=remove_cart_item]')
            ))
        return self

    def delete_item_by_index(self, index):
        """
        :param index: starts with 0
        :return: self
        """
        if self.is_empty:
            raise exceptions.DeleteItemFromCartException('Cannot delete item: cart is empty')
        cart_items = self.__driver.find_elements_by_css_selector('div#box-checkout-cart button[name=remove_cart_item]')
        try:
            cart_items[index].click()
        except IndexError:
            raise exceptions.DeleteItemFromCartException('There is no item with index {}'.format(index))
        self.__wait_u(ec.staleness_of(cart_items[index]))
        return self

    @property
    def is_empty(self):
        element = self.__wait_u(ec.presence_of_element_located((By.CSS_SELECTOR, 'div#box-checkout > div.cart > *')))
        return not element.get_attribute('id') == 'box-checkout-cart'
