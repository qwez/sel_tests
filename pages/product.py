from helpers import exceptions
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By


class ProductPopUp:

    def __init__(self, driver):
        self.__driver = driver
        self.is_closed = False
        self.__wait_u = WebDriverWait(self.__driver, 10).until

    def close(self):
        self.__driver.find_element_by_css_selector('button[aria-label="Close"]').click()
        self.__wait_u(ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.featherlight.active')))
        self.is_closed = True

    def add_to_cart(self, options):
        if self.is_closed:
            raise exceptions.ProductPopUpIsClosed('Pop-up is closed')
        if len(self.options):
            if options:
                self.select_options(options)
            else:
                raise exceptions.FailedAddProductToCart('Select options before adding product to cart')
        else:
            if options and type(options) == dict:
                raise exceptions.FailedAddProductToCart('You are trying to select prod. options, but there are no any')
        self.__driver.find_element_by_css_selector('button[name=add_cart_product]').click()
        return self

    def select_options(self, options):
        for option in self.options:
            option_name = option.get_attribute('name')[8:-1]
            if type(options) == dict:
                Select(option).select_by_visible_text(options[option_name])
            else:
                Select(option).select_by_index(1)
        # TODO: сделать проверку все ли опшены выбраны и не осталось ли лишних

    @property
    def options(self):
        return self.__driver.find_elements_by_css_selector('select[name^=options]')

    @property
    def name(self):
        return self.__driver.find_element_by_css_selector('div.featherlight-content div#box-product h1.title').text
