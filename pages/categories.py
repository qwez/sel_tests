from helpers import stuff, exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from pages import product
import logging


class Categories:

    def __init__(self, driver, domain):
        self.__driver = driver
        self._url = domain + '/categories'
        self.opened_category = None
        self.last_opend_product = None
        self.__wait_u = WebDriverWait(self.__driver, 10).until

    def __check_category(self):
        logging.getLogger('my_logger').debug('cheking category')
        if not self.opened_category:
            raise exceptions.GetProductInCategoryException('Category is not opened yet')
        bread_crumbs = self.__driver.find_element_by_css_selector('main#content > ul.breadcrumb').text
        if bread_crumbs[-len(self.opened_category):] != self.opened_category:
            raise exceptions.GetProductInCategoryException(
                'Current page is not a last opened category. Run open_category() method.'
            )

    def open_home(self):
        self.__driver.get(self._url)
        return self

    def open_category(self, cat_name):
        if stuff.cut_last_slash(self.__driver.current_url) != self._url:
            self.open_home()
        try:
            self.__driver.find_element_by_xpath('//div[@id="box-categories"]//h3[text()="{}"]'.format(cat_name)).click()
        except:
            raise exceptions.OpenCategoryException('Error while openning category ' + cat_name)
        self.__wait_u(ec.visibility_of_element_located((By.CSS_SELECTOR, 'div#box-category > h1')))
        self.opened_category = cat_name
        self.last_opend_product = 0
        return self

    def iter_products(self):
        self.__check_category()
        last_retrieved_element = 0
        while True:
            all_products = self.__driver.find_elements_by_css_selector('main#content div.product div.name')
            if len(all_products) <= last_retrieved_element:
                break
            yield all_products[last_retrieved_element]
            last_retrieved_element += 1

    def open_next_product(self):
        self.__check_category()
        if self.last_opend_product is None:
            return None
        all_products = self.__driver.find_elements_by_css_selector('main#content div.product div.name')
        if len(all_products) <= self.last_opend_product:
            return None
        all_products[self.last_opend_product].click()
        self.__wait_u(ec.visibility_of_element_located((By.CSS_SELECTOR, 'div.featherlight-content div#box-product h1')))
        self.last_opend_product += 1
        return product.ProductPopUp(self.__driver)
