## Selenium tests
**Run:**
```sh
python -m pytest -v --domain=http://demo.litecart.net --hub=localhost:4444 --browser "chrome"
```
_hub_ - Selenium HUB host:port