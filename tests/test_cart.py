from tests.data_providers import Categories
from helpers.stuff import generator
import logging


class TestCheckCartWork:

    def test_1(self, application):
        category = application.open_category(Categories.for_add_to_cart)
        for product in generator(category.open_next_product):
            product\
                .add_to_cart(True)\
                .close()
            break
        cart = application.cart.open()
        while not cart.is_empty:
            cart.delete_item_by_index(0)
        assert cart.is_empty

    def setup(self):
        self.logger = logging.getLogger('my_logger_4')
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug('start test TestCheckCartWork')

    def teardown(self):
        self.logger.debug('finish test TestCheckCartWork')

