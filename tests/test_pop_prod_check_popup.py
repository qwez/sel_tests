from helpers.stuff import generator
from time import sleep
import logging


class TestCheckProductNameInPopup:

    def test_1(self, application):
        application\
            .open_shop()\
            .open_popular_products()
        for popular_product in generator(application.popular_products.get_next_product):
            name = popular_product.name
            name_on_poup = popular_product.open_popup().name
            assert name == name_on_poup
            popular_product.product_popup.close()
            break

    def setup(self):
        self.logger = logging.getLogger('my_logger_5')
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug('start test TestCheckPriductNameInPopup')

    def teardown(self):
        self.logger.debug('finish test TestCheckPriductNameInPopup')
